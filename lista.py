"""
Lista em Python
fatiamento
append, insert, pop, del, clear, extend, min, max
range
"""

# juntando listas
l1 = [1,2,3]
l2 = [4,5,6]
l3 = [7,8,9]
l4 = l1 + l2 + l3
print(l4)


# extend
# uma lista que vai extender de outra

l5 = ['arroz', 'carne']
l6 = ['feijao', 'suco de maracuja']
l5.extend(l6)
print(l5)

## adicionar valore em uma lista
## iseri um valor no final da lista

l5.append('carvao')
print(l5)

## insert posso inserir valor aonde eu quiser em
## uma linda, basta eu colcoar em qual indice vai
## ficar, e depois colocar o valor que vai ser
## atribuido

l6.insert(1, 'mocoto')
print(l6)

## deletar o ultimo elemento da lista
l6.pop()
print(l6)

## del, com o del eu posso colocar o valor
## do indice ao qual eu quero excluir
del(l1[0])
print(l1)

## pegando o valor maximo de uma linsta
print(max(l2))

## valor minimo
print(min(l3))

## usando o ranqe, lembrando que aqui em lista
## ele tem que vir com um antecessor dele
## que e a funcao list
## aqui ele vai de 1 a 9
## para chegar ao 10 eu tenho que colocar
## de 1 a 11

l9 = list(range(1, 10))
print(l9)
