perguntas = {
    'Pergunta 1': {
        'pergunta': 'Quanto e 2+2 ',
        'resposta': {
            'a': '1',
            'b': '2',
            'c': '10',
        },
        'resposta_certa': 'b',
    },
    'Pergunta 2': {
        'pergunta': 'Qual e o animal? ',
        'resposta': {
            'jacare': 'talvez',
            'rolinha': 'sim',
        },
        'resposta_certa': 'rolinha',
    },
}

print()

respostaCorreta = 0

for pk, pv in  perguntas.items():
    print(f'{pk}: {pv["pergunta"]}')


    print('Respostas')
    for res, rv in pv['resposta'].items():
        print(f'[{res}]: {rv}')

    res_user = input('Escolha uma das alternativas: ')

    if res_user == pv['resposta_certa']:
        print('Mizeravel Acertou!!')
        respostaCorreta +=1
    else:
        print('Bicho burro do caralho!!!')

    print()

qtd_perguntas = len(perguntas)
acerto = respostaCorreta / qtd_perguntas * 100
print(f'Quantidade de acerto foi de: {acerto}')
