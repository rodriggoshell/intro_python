import os

caminho = input('Digite o caminho do arquivo que deseja: ')
buscar_termo = input('Digite o termo que voce ta buscando: ')

cont = 0

def formatTamanho(tamanho):
    base = 1024
    kilo = base
    mega = base ** 2
    giga = base ** 3
    tera = base ** 4
    peta = base ** 5

    if tamanho < kilo:
        texto = 'B'
    elif tamanho < mega:
        tamanho /= kilo
        texto = 'K'
    elif tamanho < giga:
        tamanho /= mega
        texto = 'M'
    elif tamanho < tera:
        tamanho /= giga
        texto = 'G'
    elif tamanho < peta:
        tamanho /= tera
        texto = 'T'
    else:
        tamanho /= peta
        texto = 'P'
    tamanho = round(tamanho, 2)
    return f'{tamanho}{texto}'.replace('.', ',')

for raiz, diretorios, arquivos in  os.walk(caminho):
    for arquivo in arquivos:
        if buscar_termo in arquivo:
            try:
                cont+=1
                caminho_completo = os.path.join(raiz, arquivo)
                nome_arquivo, extensao_arquivo = os.path.splitext(arquivo)
                tamanho = os.path.getsize(caminho_completo)

                print()
                print('Encotei o aquivo:', arquivo)
                print('Caminho:', caminho_completo)
                print('Nome do arquivo:', nome_arquivo)
                print('Extensao:', extensao_arquivo)
                print('Tamanho:', tamanho)
                print('Tamanho formatado:', formatTamanho(tamanho))
            except PermissionError as e:
                print('Precisa da senha de administrador!')
print()
print(f'{cont} arquivo(s) encontrado(s)')
