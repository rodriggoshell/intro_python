"""
add (adiciona), update (atualiza), clear, dicard
# union | (une)
# intersection & (todos os elementos presentes nos dois sets)
# difference - (elementos apenas no set da esquerda)
# symmetric_difference ^ (elementos que estao nos dois sets, mas nao em ambos)
---------------------------------------------------------------------------------------------------------
um sets e como se fosse um dicionario, porem se estiver vazio ele deixa de ser um sets e passa a ser
um dicionario. so que sem chave e valor
quando eu quiser usar um sets vazio eu tenho que colcoar um variavel = set()
e caso eu queira add algo nele basta fazer assim: variavel.add('meu pau')

"""

variavel = {1,21,2,1}
#para excluir basta usar o discard
variavel.discard(21)
print(variavel)

var1 = set()
var1.add('Rodrigo Martins')
print(var1)
