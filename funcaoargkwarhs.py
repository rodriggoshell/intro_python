"""
Funcoes (def) em python - args kwargs
"""

def func(a1,a2,a3,a4, nome=None):
    print(a1, a2, a3, a4, nome)

func(2,4,5,6,nome='Rodrigo')


## quando eu nao sei quantos argumentos
## vao ser colocado dentro da funcao
## *args -> nao precisa ser args, pode ser
## **kwargs -> argumento com palavras chaves
## qualquer outro nome
## retorna uma tuple(tupla)

def arg(*args):
    print(args)
    print(args[2])
arg(1,2,5,4,5,6,5,)
