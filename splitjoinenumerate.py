# split, join, enumerate
# split -> Dividir uma string
# join -> junta uma lista
# enumerate -> enumera elementos da lista(array)
#

var = "Lula ladrao, e bolsonaro louco"
lista = var.split(' ')
print(var)
print(lista)


# o join transforma uma linsta em um text
# e nesse caso ele vai ser separado por virgula
# que ta dentro das aspas
let = ['ohhhh', 'lasqueira', 'so', 'mode com me']
let2 = ','.join(let)
print(let2)



# enumerate serve para enumerar os indices da lista que
# que e conhecida como array
for indice, valor in enumerate(lista):
    print(indice, valor)
