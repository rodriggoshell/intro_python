import os
import shutil

caminho_pasta = input('Digite o que voce quer mover dessa pasta para um novo diretorio ou um ja existente: ')
caminho_novo = input('Novo caminho do diretorio para onde voce quer copiar ou mover o seu arquivo: ')


try:
    os.mkdir(caminho_novo)
except FileExistsError:
    print(f'Pasta{caminho_novo} ja existe.')
for root, dirs, files in os.walk(caminho_pasta):
    for file in files:
        old_file_path = os.path.join(root, file)
        new_file_path = os.path.join(caminho_novo, file)
        print(old_file_path)

        shutil.move(old_file_path, new_file_path)
        print(f'Arquivo {file} movido com sucesso!')
